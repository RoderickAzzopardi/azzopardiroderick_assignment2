package com.example.azzopardiroderick_assignment2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnBack = findViewById(R.id.btnBack);

        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra("imageUrl");
        String gameTitle = intent.getStringExtra("gameTitle");
        String gameStory = intent.getStringExtra("gameStory");
        String gameDesc = intent.getStringExtra("gameDesc");

        ImageView imageView = findViewById(R.id.image_view_detail);
        TextView textViewTitle = findViewById(R.id.text_view_title);
        TextView textViewStory = findViewById(R.id.text_view_story);
        TextView textViewDesc = findViewById(R.id.text_view_summary);

        Picasso.with(this).load(imageUrl).fit().centerInside().into(imageView);
        textViewTitle.setText(gameTitle);
        textViewStory.setText(gameStory);
        textViewDesc.setText(gameDesc);
    }

    public void goBack(View v){
        Intent intent = this.getIntent();
        String strdata = intent.getExtras().getString("Uid");

        if(strdata.equals("Search_Activity")){
            startActivity(new Intent(DetailsActivity.this, SearchActivity.class));
        } else if (strdata.equals("Recent_Activity")){
            startActivity(new Intent(DetailsActivity.this, RecentGamesActivity.class));
        } else if (strdata.equals("Upcoming_Activity")){
            startActivity(new Intent(DetailsActivity.this, UpcomingGamesActivity.class));
        }

    }
}
