package com.example.azzopardiroderick_assignment2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class IGDBAdapter extends RecyclerView.Adapter<IGDBAdapter.IGDBViewHolder> {
    private Context context;
    private List<GameItem> mGameList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemclick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public IGDBAdapter(List<GameItem> gameList, Context con){
        context = con;
        mGameList = gameList;
    }

    @NonNull
    @Override
    public IGDBViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.game_item, viewGroup, false);
        return new IGDBViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull IGDBViewHolder holder, int position) {
        GameItem currentItem = mGameList.get(position);

        //Bitmap gameCover = currentItem.getImage();
        String gameCover = currentItem.getImage();
        String gameTitle = currentItem.getTitle();
        //holder.mImageView.setImageBitmap(gameCover);
        Picasso.with(context).load(gameCover).fit().centerInside().into(holder.mImageView);
        holder.mTextViewTitle.setText(gameTitle);

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        try {
            return mGameList.size();
        }
        catch (NullPointerException e){
            return 0;
        }
    }

    public class IGDBViewHolder extends RecyclerView.ViewHolder {
        public CardView cv;
        public ImageView mImageView;
        public TextView mTextViewTitle;

        public IGDBViewHolder(View itemView) {
            super(itemView);

            cv = itemView.findViewById(R.id.cv);
            mImageView = itemView.findViewById(R.id.game_photo);
            mTextViewTitle = itemView.findViewById(R.id.game_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            mListener.onItemclick(position);
                        }
                    }
                }
            });
        }
    }
}
