package com.example.azzopardiroderick_assignment2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SearchActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IGDBAdapter.OnItemClickListener {
    public static final String EXTRA_URL = "imageUrl";
    public static final String EXTRA_TITLE = "gameTitle";
    public static final String EXTRA_STORY = "gameStory";
    public static final String EXTRA_DESC = "gameDesc";

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String GAME_TITLE = "gameSearch";
    String dateTime = DateFormat.getDateTimeInstance().format(new Date());
    Snackbar snackbar;

    private RecyclerView mRecyclerView;
    private List<GameItem> games;
    private String gameTitle;
    TextView txtSearch;
    EditText editText;
    Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_search);

        editText = findViewById(R.id.txtSearch);
        txtSearch = findViewById(R.id.txtSearch);
        btnSearch = findViewById(R.id.btnSearch);

        mRecyclerView = findViewById(R.id.rvDisplay);
        LinearLayoutManager llm = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL));

        snackbar = Snackbar.make(drawer, "Last Activity was on: "+dateTime, Snackbar.LENGTH_LONG)
                .setAction("Action", null);
        View view = snackbar.getView();
        view.setBackgroundColor(Color.parseColor("#008577"));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_search) {
            Intent i = new Intent(this, SearchActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
        } else if (id == R.id.nav_recent) {
            Intent i = new Intent(this, RecentGamesActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
        } else if (id == R.id.nav_upcoming) {
            Intent i = new Intent(this, UpcomingGamesActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
        } else if (id == R.id.nav_about) {
            Intent i = new Intent(this, AboutActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showGames(View v){
        closeKeyboard();

        String searchGame = "games/?search="+txtSearch.getText().toString()+"&fields=cover.url,name,storyline,summary";
        GetIGDBTask task = new GetIGDBTask();

        if(NetworkCheck.isNetworkConnected(this)){
            try{
                games = task.execute(searchGame).get();
                if(games == null){
                    mRecyclerView.setVisibility(View.INVISIBLE);
                    Toast.makeText(this, "Game not found!", Toast.LENGTH_LONG).show();
                }
                else{
                    IGDBAdapter adapter = new IGDBAdapter(games, this);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mRecyclerView.setAdapter(adapter);
                    adapter.setOnItemClickListener(this);

                    txtSearch.setText("");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        else{
            Toast.makeText(this, "No Internet Connection Detected!", Toast.LENGTH_LONG).show();
        }
    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if(view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onItemclick(int position) {
        Intent detailsIntent = new Intent(this, DetailsActivity.class);
        detailsIntent.putExtra("Uid","Search_Activity");
        GameItem clickedGame = games.get(position);

        detailsIntent.putExtra(EXTRA_URL, clickedGame.getImage());
        detailsIntent.putExtra(EXTRA_TITLE, clickedGame.getTitle());
        detailsIntent.putExtra(EXTRA_STORY, clickedGame.getStory());
        detailsIntent.putExtra(EXTRA_DESC, clickedGame.getDesc());

        startActivity(detailsIntent);
    }

    public void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(GAME_TITLE, txtSearch.getText().toString());
        editor.commit();
    }

    public void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        gameTitle = sharedPreferences.getString(GAME_TITLE, "");

        txtSearch.setText(gameTitle);
    }

    @Override
    protected void onPause() {
        super.onPause();

        saveData();
    }

    protected void onStop() {
        super.onStop();

        saveData();
    }

    protected void onResume(){
        super.onResume();

        loadData();
        snackbar.show();
    }

    protected void onStart(){
        super.onStart();

        loadData();
        snackbar.show();
    }
}
