package com.example.azzopardiroderick_assignment2;

public class GameItem {
    public String g_image;
    public String g_title;
    public String g_story;
    public String g_desc;

    public GameItem()
    {

    }

    public GameItem(String image, String title, String story, String desc){
        g_image = image;
        g_title = title;
        g_desc = desc;
        g_story = story;
    }

    public String getImage(){
        return g_image;
    }

    public String getTitle(){
        return g_title;
    }

    public String getStory(){ return g_story; }

    public String getDesc() { return g_desc; }
}
