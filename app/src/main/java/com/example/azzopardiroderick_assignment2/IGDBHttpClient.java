package com.example.azzopardiroderick_assignment2;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class IGDBHttpClient {
    private static String BASE_URL = "https://api-v3.igdb.com/";
    private static String APP_ID = "83dd3d9e397fd92f93293d47242f2004";

    public String getGameData(String title) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .header("user-key", APP_ID)
                .url(BASE_URL + title)
                .build();

        Response response;

        try{
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

}
