package com.example.azzopardiroderick_assignment2;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetIGDBTask extends AsyncTask<String, Void, List<GameItem>> {

    @Override
    protected List<GameItem> doInBackground(String... title) {

        IGDBHttpClient client = new IGDBHttpClient();

        String resultJSON = client.getGameData(title[0]);

        try {
            List<GameItem> games = new ArrayList<>();

            JSONArray jArr = new JSONArray(resultJSON);

            String imgUrl;

            for (int i = 0; i < jArr.length(); i++){
                    GameItem myGame = new GameItem();

                    JSONObject jObject = jArr.getJSONObject(i);

                    if(jObject.has("cover")){
                        imgUrl = "https://" + jObject.getJSONObject("cover").getString("url").substring(2);
                    }
                    else {
                        imgUrl = "http://www.uidownload.com/files/74/156/996/controller-game-controller-video-game-icon.png";
                    }
                    myGame.g_image = imgUrl;
                    /*
                    try {
                        InputStream is = new URL(imgUrl).openStream();
                        myGame.g_image = BitmapFactory.decodeStream(is);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                    myGame.g_title = jObject.getString("name");

                    if(jObject.has("storyline")){
                        myGame.g_story = jObject.getString("storyline");
                    }else{
                        myGame.g_story = "N/a";
                    }

                    if(jObject.has("summary")){
                        myGame.g_desc = jObject.getString("summary");
                    }else{
                        myGame.g_desc = "N/a";
                    }

                    games.add(myGame);
            }

            return games;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
